require './init'
require './domain/stock_comparison'
require './domain/stock_comparator'

companies = %w(GOOG AMZN EBAY LNKD FB YHOO AAPL MSFT P YELP)

adapter = Yql.finance
companies.each { |c| adapter.stock_symbol(c) }
adapter.date_interval(Date.parse('2014-01-02'), Date.parse('2015-01-02'))

response = adapter.pull_in

stocks_collection = companies.each_with_object([]) do |quote_name, collection|
  collection << response.all_from(symbol: quote_name)
end

comparator = StockComparator.new(stocks_collection)
comparator.sort_by_return_desc.each do |ordered|
  puts "#{ordered.percentage_in_period} - #{ordered.quote_symbol}"
end
