require 'nokogiri'
require File.join(ENV['APP_PATH'], 'domain', 'lib', 'quote')

module Yql
  class Response
    attr_reader :raw_responses

    def initialize
      @raw_responses = []
      @parsed_responses = []
    end

    def add_response(xml_response)
      @raw_responses << xml_response
    end

    def parsed_responses(parser: Nokogiri)
      raw_responses.each_with_object([]) do |raw_response, collection|
        collection << parser::XML(raw_response)
      end
    end

    def symbol?(name)
      parsed_responses.each_with_object([]) do |response, collection|
        collection << response.xpath("//@Symbol='#{name}'")
      end.include?(true)
    end

    def all_from(symbol: name)
      parsed_responses.each_with_object([]) do |response, collection|
        response.xpath("//quote[@Symbol='#{symbol}']").each do |node|
          collection << build_quote_from_xml_element(node)
        end
      end
    end

    def size
      parsed_responses.map do |response|
        response.xpath("//query/@yahoo:count").first.value.to_i
      end.reduce(:+)
    end

    private
    def build_quote_from_xml_element(node)
      Quote.new(node.attributes['Symbol'].value)
        .define_values_from(
          node.children.each_with_object({}) do |child, hash_table|
            next unless child.element?
            hash_table[child.name.downcase.to_sym] = child.children.to_s
          end
        )
    end
  end
end
