require 'faraday'

module Yql
  class Finance
    attr_reader :driver, :default_params, :query_dates

    def initialize(adapter: ::Faraday, api_endpoints: {}, yql_response: Yql::Response.new)
      @default_params = api_endpoints
      @query_string = {where: {}}
      @driver = adapter.new(url: @default_params['base_url'])
      @query_dates = []
      @response = yql_response
    end

    def stock_symbol(name)
      add_query(yql_name: :symbol, value: name)
    end

    def date_interval(start_date, end_date)
      add_date(start_date)
      add_date(end_date)
    end

    def pull_in
      yql_dump.each do |query|
        build_response(@driver.get(@default_params['api_version'], generate_query_string(query)))
      end

      @response
    end

    def yql_dump
      return "" if queries.empty?

      if query_dates.any?
        query_dates.each_with_object([]) do |date, collection|
          collection << sprintf("select * from %s where %s %s", @default_params['finance_table'], where_clause_parsed_to_string, date_interval_parsed_to_string(date))
        end
      else
        [sprintf("select * from %s where %s", @default_params['finance_table'], where_clause_parsed_to_string)]
      end
    end

    private
    def build_response(raw_response)
      if raw_response.status != 200
        raise RuntimeError, "YQL Connection died with status: #{raw_response.status} - #{raw_response.body}"
      end

      @response.add_response(raw_response.body)
    end

    def add_query(yql_name: nil, value: name)
      if queries.select { |field, value| field == yql_name }.empty?
        @query_string[:where][yql_name] = [value]
      else
        unless @query_string[:where][yql_name].include?(value)
          @query_string[:where][yql_name] << value
        end
      end
    end

    def add_date(date)
      @query_dates << date
    end

    def generate_query_string(query_string)
      { q: query_string, env: @default_params['source'] }
    end

    def queries
      @query_string[:where]
    end

    def where_clause_parsed_to_string
      queries.each.with_object([]) do |(key,value), arr|
        if value.size > 1
          arr << "#{key} in (#{value.map { |e| "'#{e}'" }.join(', ')})"
        else
          arr << "#{key} = '#{value.first}'"
        end
      end.join(' and ')
    end

    def date_interval_parsed_to_string(date)
      sprintf "and startDate = '%s' and endDate = '%s'", date, date
    end
  end
end
