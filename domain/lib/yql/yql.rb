require 'yaml'
require 'faraday'

module Yql
  def self.finance
    Finance.new(
      api_endpoints: YAML.load_file(File.join(ENV['APP_PATH'], 'config', 'yql_attributes.yml'))['yql'],
      adapter: Faraday
    )
  end
end
