class Quote
  attr_reader :symbol

  def initialize(symbol)
    @symbol = symbol
    @values = {}
  end

  def define_values_from(hash)
    @values[:date]    = Date.parse(hash.fetch(:date))
    @values[:open]    = hash.fetch(:open).to_f
    @values[:high]    = hash.fetch(:high).to_f
    @values[:low]     = hash.fetch(:low).to_f
    @values[:close]   = hash.fetch(:close).to_f
    @values[:volume]  = hash.fetch(:volume).to_i
    @values[:adjust_close] = hash.fetch(:adj_close).to_f
    self
  end

  [:date, :open, :high, :low, :close, :volume, :adjust_close].each do |value|
    define_method value.to_sym do
      @values[value.to_sym]
    end
  end
end
