class StockComparison

  def initialize(initial_quote, end_quote)
    @initial_quote = initial_quote
    @end_quote     = end_quote
  end

  def percentage_in_period
    ("%.3f" % (@end_quote.adjust_close / @initial_quote.adjust_close - 1)).to_f
  end

  def quote_symbol
    @initial_quote.symbol
  end
end
