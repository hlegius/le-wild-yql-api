class StockComparator

  def initialize(stocks_collection, comparison_class: StockComparison)
    @stocks = stocks_collection
    @comparison_class = comparison_class
  end

  def sort_by_return_desc
    compare.sort { |ca,cb| cb.percentage_in_period <=> ca.percentage_in_period }
  end

  private
  def compare
    @stocks.each_with_object([]) do |stock_by_symbol, collection|
      next if stock_by_symbol.empty?
      collection << @comparison_class.new(stock_by_symbol.first, stock_by_symbol.last)
    end
  end
end
