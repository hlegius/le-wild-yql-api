require 'sinatra'
require 'json'
require './init'
require './domain/stock_comparison'
require './domain/stock_comparator'

get '/ranking/:start_date/:end_date' do
  companies = %w(GOOG AMZN EBAY LNKD FB YHOO AAPL MSFT P YELP)

  adapter = Yql.finance
  companies.each { |c| adapter.stock_symbol(c) }
  adapter.date_interval(Date.parse(params[:start_date]), Date.parse(params[:end_date]))

  response = adapter.pull_in

  stocks_collection = companies.each_with_object([]) do |quote_name, collection|
    collection << response.all_from(symbol: quote_name)
  end

  StockComparator.new(stocks_collection).sort_by_return_desc.map do |o|
    { stock: o.quote_symbol, value: o.percentage_in_period }
  end.to_json
end

get '/' do
  haml :index
end
