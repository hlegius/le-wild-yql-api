# README #

This project is just a demo for how to implement YQL Finance with TDD. There is a small frontend with Sinatra and Jquery to fetch data.

### What is this repository for? ###

* Coding Test
* Test-First isn't nothing hard
* YQL Finance test out

### How do I get set up? ###

* Clone
* Run `rspec` inside root directory
* Be happy.
* Read the code.
* Send me questions about this. Find me on Twitter [@hlegius](https://twitter.com/hlegius)

### Contribution guidelines ###

* Improve some code with Test-First practice.
* Open a PR.
* Wait and be happy, you did it!