require './domain/stock_comparator'

describe StockComparator do
  let(:stocks_collection) do
    [
      [double('YHOO'), double('YHOO'), double('YHOO')],
      [double('AMZN'), double('AMZN'), double('AMZN')],
      [double('YELP'), double('YELP'), double('YELP')]
    ]
  end
  let(:stock_comparison) { double('StockComparison') }

  subject { StockComparator.new(stocks_collection, comparison_class: stock_comparison) }

  let(:yhoo) { double('YHOO', percentage_in_period: 1) }
  let(:amzn) { double('AMZN', percentage_in_period: 3) }
  let(:yelp) { double('YELP', percentage_in_period: 2) }

  before do
    allow(stock_comparison).to receive(:new).and_return(yhoo, amzn, yelp)
  end

  it 'sorting values by % of return desc' do
    expect(subject.sort_by_return_desc).to eq [amzn, yelp, yhoo]
  end
end

