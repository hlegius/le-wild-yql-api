require './domain/stock_comparison'

describe StockComparison do
  let(:initial_quote) { double('QuoteA', adjust_close: 10.00) }
  let(:end_quote)     { double('QuoteB', adjust_close: 20.00) }

  describe 'when calculating percentage of return' do
    subject { StockComparison.new(initial_quote, end_quote) }

    it 'will be 100% of return' do
      expect(subject.percentage_in_period).to eq 1.000
    end
  end
end
