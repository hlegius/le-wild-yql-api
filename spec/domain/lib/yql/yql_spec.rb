require './domain/lib/yql/yql'

describe Yql do
  let(:yql_attributes_file) do
    YAML.load_file(File.join(ENV['APP_PATH'], 'config', 'yql_attributes.yml'))['yql']
  end

  it '.finance' do
    expect(Yql::Finance).to receive(:new).with(api_endpoints: yql_attributes_file, adapter: Faraday)
    subject.finance
  end
end
