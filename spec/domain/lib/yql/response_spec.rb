require './domain/lib/yql/response'

describe Yql::Response do
  let(:xml_response) { File.read('./spec/fixtures/yql_finance_response.xml') }

  describe 'when lookup for specific symbol data' do
    before do
      subject.add_response xml_response
    end

    it { expect(subject).to be_symbol('YHOO') }
    it { expect(subject).to_not be_symbol('WTF') }

    it '.all_from(symbol: "YHOO")' do
      expect(subject.all_from(symbol: 'YHOO')).to_not be_empty
      expect(subject.all_from(symbol: 'YHOO').size).to eq(252)
    end

    it '.size will return sum of quotes (read from query attribute)' do
      expect(subject.size).to eq 252
    end
  end

  describe 'when adding multiple XML Responses' do
    before do
      subject.add_response xml_response
      subject.add_response xml_response
    end

    it 'will merge responses to turn into one big xml chunck' do
      expect(subject.size).to eq 252 * 2
    end

    it '.raw_responses' do
      expect(subject.raw_responses.size).to eq 2
    end

    it '.all_from(symbol: "YHOO")' do
      expect(subject.all_from(symbol: 'YHOO')).to_not be_empty
      expect(subject.all_from(symbol: 'YHOO').size).to eq(252 * 2)
    end
  end
end
