require './domain/lib/yql/finance'
require 'yaml'

describe Yql::Finance do
  let(:yql_attributes_file) do
    YAML.load_file(File.join(ENV['APP_PATH'], 'config', 'yql_attributes.yml'))['yql']
  end

  let(:adapter) { double('Faraday') }

  describe 'when configure yql Finance object' do

    it '.initialize with API default params' do
      expect(adapter).to receive(:new).with(url: yql_attributes_file['base_url'])
      Yql::Finance.new(adapter: adapter, api_endpoints: yql_attributes_file)
    end

    it 'will load Yql Finance base url' do
      subject = Yql::Finance.new(api_endpoints: yql_attributes_file)
      expect(subject.default_params.values).to match_array yql_attributes_file.values
    end
  end

  describe 'when set it up pull in queries' do
    it '.stock_symbol' do
      expect { subject.stock_symbol('YHOO') }.to change { subject.send(:queries) }
        .from({})
        .to({symbol: ['YHOO']})
    end

    it '.stock_symbol will support more than one stock symbol' do
      subject.stock_symbol('YHOO')
      subject.stock_symbol('AMZN')
      expect(subject.send(:queries)).to include({symbol: ['YHOO', 'AMZN']})
    end

    it '.stock_symbol will not duplicate' do
      subject.stock_symbol('YHOO')
      expect { subject.stock_symbol('YHOO') }.to_not change { subject.send(:queries) }.from({symbol: ['YHOO']})
    end

    it '.date_interval' do
      start_date = Date.parse('2014-01-01')
      end_date   = Date.parse('2015-01-01')

      expect { subject.date_interval(start_date, end_date) }.to change { subject.send(:query_dates) }
        .from([])
        .to([start_date, end_date])
    end
  end

  describe 'when parsing to yql string' do
    before do
      allow(adapter).to receive(:new).and_return spy('FaradayConnection')
    end

    subject { Yql::Finance.new(adapter: adapter, api_endpoints: yql_attributes_file) }

    it '.yql_dump' do
      subject.stock_symbol('YHOO')
      expect(subject.yql_dump).to eql(
        [
          "select * from #{yql_attributes_file['finance_table']} where symbol = 'YHOO'"
        ]
      )
    end

    it '.yql_dump with date interval' do
      subject.stock_symbol('YHOO')
      subject.date_interval(Date.parse('2014-01-02'), Date.parse('2015-01-03'))
      expect(subject.yql_dump).to eql(
        [
          "select * from #{yql_attributes_file['finance_table']} where symbol = 'YHOO' and startDate = '2014-01-02' and endDate = '2014-01-02'",
          "select * from #{yql_attributes_file['finance_table']} where symbol = 'YHOO' and startDate = '2015-01-03' and endDate = '2015-01-03'"
        ]
      )
    end

    it '.yql_dump with more than one Symbol' do
      subject.stock_symbol('YHOO')
      subject.stock_symbol('FB')
      expect(subject.yql_dump).to eql(
        [
          "select * from #{yql_attributes_file['finance_table']} where symbol in ('YHOO', 'FB')"
        ]
      )
    end

    it '.yql_dump without query' do
      expect(subject.yql_dump).to be_empty
    end
  end

  describe 'when pull in' do
    before do
      allow(adapter).to receive(:new).and_return spy('FaradayConnection', status: 200)
    end

    subject { Yql::Finance.new(adapter: adapter, api_endpoints: yql_attributes_file) }

    it '.pull_in will send correct query string to Y! Api' do
      subject.stock_symbol('YHOO')
      subject.date_interval(Date.parse('2014-01-01'), Date.parse('2015-01-01'))
      subject.pull_in
      expect(subject.driver).to have_received(:get)
        .with(
          yql_attributes_file['api_version'],
          {
            q: "select * from #{yql_attributes_file['finance_table']} where symbol = 'YHOO' and startDate = '2014-01-01' and endDate = '2014-01-01'",
            env: "store://datatables.org/alltableswithkeys"
          }
        )
      expect(subject.driver).to have_received(:get)
        .with(
          yql_attributes_file['api_version'],
          {
            q: "select * from #{yql_attributes_file['finance_table']} where symbol = 'YHOO' and startDate = '2015-01-01' and endDate = '2015-01-01'",
            env: "store://datatables.org/alltableswithkeys"
          }
        )
    end
  end
end
