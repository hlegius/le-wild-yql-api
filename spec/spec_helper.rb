ENV["RACK_ENV"] = 'test'

require File.join(File.dirname(__FILE__), '..', 'config')
require 'rspec'
require 'pry'

RSpec.configure do |config|
  config.order = "random"
  config.mock_with :rspec
end
